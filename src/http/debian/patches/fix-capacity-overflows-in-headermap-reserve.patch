From 81ceb611cf96abe91d91693e813cd5ee36cdae02 Mon Sep 17 00:00:00 2001
From: Sean McArthur <sean@seanmonstar.com>
Date: Mon, 25 Nov 2019 15:54:04 -0800
Subject: Fix capacity overflows in HeaderMap::reserve
 The patch required minimal adaption from upstream because the surrounding
 code had changed in upstream `master` branch over the 0.1.19 release.
 .
 Contrary to what one might assume with knowledge of `assert()` in C, the
 rust `assert!()` macro never gets removed for optimization, and is always
 checked resulting in a `panic!()` and thus a controlled shutdown of the
 process as described in
 https://doc.rust-lang.org/std/macro.assert.html#uses.
Origin: upstream, https://github.com/hyperium/http/commit/81ceb611cf96abe91d91693e813cd5ee36cdae02
Bug: https://github.com/hyperium/http/issues/352
Bug-Debian: https://bugs.debian.org/969896

--- a/src/header/map.rs
+++ b/src/header/map.rs
@@ -628,6 +628,9 @@
 
         if cap > self.indices.len() {
             let cap = cap.next_power_of_two();
+            assert!(cap < MAX_SIZE, "header map reserve over max capacity");
+            assert!(cap != 0, "header map reserve overflowed");
+
 
             if self.entries.len() == 0 {
                 self.mask = cap - 1;
--- a/tests/header_map.rs
+++ b/tests/header_map.rs
@@ -38,6 +38,22 @@
 }
 
 #[test]
+#[should_panic]
+fn reserve_over_capacity() {
+    // See https://github.com/hyperium/http/issues/352
+    let mut headers = HeaderMap::<u32>::with_capacity(32);
+    headers.reserve(50_000); // over MAX_SIZE
+}
+
+#[test]
+#[should_panic]
+fn reserve_overflow() {
+    // See https://github.com/hyperium/http/issues/352
+    let mut headers = HeaderMap::<u32>::with_capacity(0);
+    headers.reserve(std::usize::MAX); // next_power_of_two overflows
+}
+
+#[test]
 fn drain() {
     let mut headers = HeaderMap::new();
 
